# Continuité pédagogique

**Ce projet est destiné à assurer la continuité pédagogique avec mes élèves
pendant le confinement d'avril-mai 2021. Si vous ne savez pas que vous êtes
un de mes élèves, vous pouvez consulter les exercices et explications que
j'y mets.**

## Consignes générales

**Communiquez avec moi par courrier électronique** de préférence. Vous avez
mon adresse électronique.

**Évitez les pièces jointes.** Elles sont lourdes et pénibles à manipuler en
grand nombre.

**Évitez les photos** de vos brouillons et exercices.

**Rédigez vos exercices dans un traitement de texte** et gardez-les dans des
fichiers bien rangés.

**N'utilisez pas de présentation**, tableaux, gras, italique, caractères
spéciaux, etc.

**Copiez-collez votre exercice dans un mail** pour me le montrer.

**Pour taper des maths, faites comme avec une calculatrice bas-de-gamme**,
tapez directement avec le clavier sans chercher à ressembler à de vraies
formules.

Vous mettrez plus de temps en travaillant de cette manière que normalement,
j'en tiendrai compte dans la quantité de travail que je donnerai.

Si ce n'est fait, créez-vous un compte gratuit sur GitLab. Depuis votre
compte, activez les notifications pour ce projet (la cloche).

## Écriture des mathématiques

Voici quelques conventions pour écrire des mathématiques sur ordinateur
facilement.

Si ce que vous voulez exprimer n'est pas prévu, inventez votre propre
convention.

- Utilisez `*` pour la multiplication, à moins que vous ne saviez taper `×`.

- Pour les fractions, utilisez `/`, sans oublier des parenthèses pour savoir
  jusqu'où va la fraction. Par exemple $`\dfrac{x^2-1}{x^2+1}`$ peut
  s'écrire `(x²-1)/(x²+1)`.

- Il y a une touche carré `²` en haut à gauche. Sinon, utilisez l'accent
  circonflexe `^` pour les puissances. Par exemple $`x^3+x^2+1`$ peut
  s'écrire `x^3+x^2+1` ou `x^3+x²+1`.

- Pour la racine carrée, écrivez sqrt, comme square root. Par exemple
  $`\sqrt{\dfrac{x}{y}}`$ peut s'écrire `sqrt(x/y)`.

- N'hésitez pas à utiliser des espaces pour rendre les formules plus
  lisibles en séparant les parties.

## Premiers exercices

Pour le mercredi 7 avril, à 20:00 au plus tard, pour tous les élèves :

Faire l'exercice correspondant à votre classe, en prenant soin de respecter
toutes les consignes. Envoyez-moi votre mail tôt, de manière à ce que je
puisse répondre. Si je vous critique sur la manière de faire, recommencez
l'envoi.

À l'heure limite, je dois avoir reçu un courrier électronique respectant les
consignes avec l'exercice fait dedans, de la part de chacun d'entre vous.

### Premier exercice 205

Cf. [205.md](205.md).

### Premier exercice terminale générale

Cf. [tg.md](tg.md).

### Premier exercice terminale STMG

Cf. [stmg.md](stmg.md).
