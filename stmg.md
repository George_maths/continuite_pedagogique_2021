# Terminale STMG

Rappel des consignes générales : [README.md](README.md).

Le travail est toujours à rendre avant 20:00 du jour annoncé.

## Pour le 2021-04-07 : permier exercice

Une entreprise a un *open space* avec 25 ordinateurs pour ses employés, mais
parfois certains ordinateurs sont en panne. On suppose que la panne d'un
ordinateur n'affecte pas le fonctionnement des autres.

On note X le nombre d'ordinateurs en panne à un instant donné.

1. Quelle loi de probabilité suit X ? Justifier et préciser le paramètre
connu.

2. On a constaté qu'en moyenne, deux ordinateurs étaient en panne à chaque
instant. Que peut-on en déduire pour E(X) ?

3. En déduire la valeur du paramètre p.

4. Quelle est la probabilité qu'au moins 20 ordinateurs soient effectivement
disponibles à un instant donné ?

### Correction

1. Si la panne d'un ordinateur n'affecte pas les autres, c'est que c'est
indépendant. Donc X suit une loi binomiale de paramètre n=25 mais p n'est
pas donné.

2. E(X)=2

3. p×25=2, donc p=2/25=0.08

4. Pour qu'au moins 20 ordinateurs soient effectivement disponibles, il faut qu'au plus 5 soient en panne.

P(X≤5)≃0.988

## Pour le 2021-04-08 : exercice complet

Cet exercice est plus difficile et moins typique. Pour le résoudre, vous
aurez probablement besoin de me poser des questions plusieurs fois. Ne vous
y prenez pas trop tard.

Un jeu de hasard dans un parc d'attractions a deux modes.

Dans le mode facile, on joue six fois, avec à chaque fois cinq chances sur
neuf de réussir, et on gagne le gros lot si on a réussi au moins quatre
fois.

Dans le mode difficile, on joue deux fois, avec à chaque fois deux chances
sur cinq de réussir, et on gagne le gros lot si on a réussi les deux fois.

**Étude du mode facile**

1. On note X le nombre de fois où on réussit si on joue en mode facile.
Quelle est la loi de X ?

2. Quelle est la probabilité qu'on gagne le gros lot si on a joué en mode
facile ?

**Étude du mode difficile**

En procédant de même, calculer la probabilité qu'on gagne le gros lot si on
a joué en mode difficile.

**Étude du jeu complet**

Le mode du jeu est choisi aléatoirement, avec une chance sur quatre que ce
soit le mode difficile.

On note F « on a joué en mode facile », D « on a joué en mode difficile » et
G « on a gagné le gros lot ».

1. Représenter la partie complète par un arbre de probabilité.

2. Calculer P(G).

3. Le gros lot coûte 50€, la partie coûte 5€. On note B le bénéfice réalisé
par le parc sur une partie. Que vaut B quand on gagne ? Quand on perd ?

4. Calculer E(B).

### Correction

**Étude du mode facile**

1. X suit une loi binomiale de paramètres n=6 et p=5/9.

2. On gagne si X≥4.

   P(X≥4) = 1 - P(X≤3) ≃ 0.45 à la calculatrice (`1-binomFRép(6,5/9,3)`)

**Étude du mode difficile**

On note Y le nombre de parties réussies. Y suit une loi binomiale de
paramètres n=2 et p=2/5.

P(Y=2) = (2/5)² = 0.16

**Étude du jeu complet**

1.

```
                   0.45
                 ———————— G
      0.75      /
    ———————— F <
   /            \  0.55
  /              ———————— ¬G
 /               
<                
 \                 0.16
  \              ———————— G
   \  0.25      /
    ———————— D < 
                \  0.84
                 ———————— ¬G
```

2. P(F∩G) = P(F) × P_F(G) = 0.75 × 0.45

   P(D∩G) = P(D) × P_D(G) = 0.25 × 0.16

   P(G) = P(F∩G) + P(D∩G) = 0.75 × 0.45 + 0.25 × 0.16 = 0.3775

3. B vaut -45 quand on gagne et +5 quand on perd.

4. E(B) = 0.3775 × (-45) + 0.7025 × (+5) = -13.745

   Ce jeu n'est pas rentable pour le parc, il est trop facile.

## Pour la rentrée

Nous commencerons les suites, donc révisez ce que vous savez à ce sujet.

Regardez ce que j'ai fait avec GitLab, créez-vous un compte gratuit et
essayez de faire pareil. (Utilisez plutôt le « web IDE » que « edit ».)

## Pour le 2021-04-29

Exercices de révision de première sur les suites.

Il est attendu de vous que vous cherchiez par vous-mêmes les méthodes si
vous avez oublié.

### Exercice 1

Pour chacune des suites suivantes, indiquez si elle peut être arithmétique
ou géométrique ou pas du tout. Si elle peut, précisez sa raison et les trois
termes qui suivent le dernier terme donné.

1. u0=7 ; u1=13 ; u2=19 ; u3=25

2. u0=100 ; u1=70 ; u2=49 ; u3=34.3

3. u0=1 ; u1=2 ; u2=3 ; u3=5

4. u0=70 ; u1=68 ; u2=66 ; u3=65

5. u0=50 ; u1=47 ; u2=44 ; u3=41

6. u0=1 ; u1=2 ; u2=4 ; u3=8

### Exercice 2

La suite (un) est géométrique. On donne u10=50 et u20≃67.196.

1. Déterminer la raison de (un).

2. Calculer u0.

3. Donner le terme général de (un).

4. Quel est le premier n tel que un dépasse 100?

### Exercice 3

La suite (vn) est arithmétique. On donne v8=1024 et v15=2144.

1. Déterminer la raison de (vn).

2. Calculer v0.

3. Donner le terme général de (vn).

4. Quel est le premier n tel que un dépasse 8192?
