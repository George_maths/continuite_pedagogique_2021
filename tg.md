# Terminale générale

Rappel des consignes générales : [README.md](README.md).

Le travail est toujours à rendre avant 20:00 du jour annoncé.

## Pour le 2021-04-07 : permier exercice

Dans un repère orthonormé, on donne A(1,1), B(10,4) et C(11,11).
H est le point de (AB) tel que (HC) soit perpendiculaire à (AB).

1. Démontrer que pour tout point M∈(AB) CM≥CH.

2. En calculant le produit scalaire de A→B et A→C de deux manières
différentes, déterminer la valeur du produit scalaire de A→B et A→H.

3. En déduire la valeur de k tel que A→H = k A→B.

4. Calculer les coordonnées de H.

5. Calculer l'aire du triangle ABC.

6. Reprendre l'exercice avec A(1,1,0), B(10,4,2) et C(11,11,-11).

## Pour le 2021-04-08 : notion de vecteur normal

Le but de cet exercice est de faire comprendre une nouvelle version du
cours. Prenez soin de bien comprendre chaque question, ce qu'elle signifie
par rapport aux autres, etc. Posez autant de questions que nécessaire par
courrier électronique.

**Première partie**

Le plan est rapporté à un repère orthonormé. On considère A(2,1) et B(6,3).

1. Trouver les coordonnées de tous les vecteurs orthogonaux au vecteur A→B.

2. Soit n un de ces vecteurs, non nul, le plus simple possible. Écrire les
coordonnées de n.

3. Démontrer que pour tout point M de (AB), on a le produit scalaire
n.A→M = 0.

4. On pose M(x,y). Écrire le produit scalaire n.A→M avec ces coordonnées et
simplifier au maximum.

La formule obtenue, avec =0 en plus, donne une équation de la droite (AB).

**Deuxième partie**

L'espace est rapporté à un repère orthonormé. On considère A(2,1,0),
B(6,3,0) et C(4,7,2).

1. Démontrer que si n est orthogonal à A→B et à A→C, il est automatiquement
orthogonal à B→C.

2. Trouver les coordonnées de tous les vecteurs orthogonaux à A→B, B→C et
C→A.

3. Soit n un de ces vecteurs, non nul, le plus simple possible. Écrire les
coordonnées de n.

4. Démontrer que pour tout point M de (ABC), on n.A→M = 0.

5. On pose M(x,z,z). Écrire le produit scalaire n.A→M avec ces coordonnées
et simplifier au maximum.

6. Comment peut-on appeler la formule obtenue, avec =0 en plus ?

## Pour le 2021-04-09 : exercice du livre

102 p. 85

## Pour le 2021-04-28

**À rendre d'abord le 2021-04-27 pour une pré-correction et des conseils.**

Soient A(7,-1) et B(-2,5).

1. Déterminer une équation de la droite (AB) sous la forme ?x+?y+?=0. Vous
prendrez soin de simplifier les facteurs entier communs.

2. Sur un repère qui va au moins de -10 à -10 sur chaque axe, tracer la
droite (AB). [Inutile de *rendre* cette question.]

3. Sur le même repère pour chaque point de coordonnées entières, calculer la
valeur de ?x+?y+? de l'équation que vous avez trouvée pour (AB) et notez le
résultat sur le graphique. Prenez soin d'écrire petit. Vous aurez donc des 0
sur (AB) et d'autres nombres ailleurs. [Inutile de *rendre* cette question.]

4. Trouver et représenter en couleur tous les points pour lesquels le
résultat est 13. Même question avec 26, 39, -13, -16, -39. [Inutile de
*rendre* cette question.]

5. Décrire ce que vous observez sur le graphique.

6. Soient le vecteur n(2,3) et le point M(x,y). Calculer et simplifier n.AM
et n.BM.

7. Soit N un point tel que n.AN=26. Démontrer qu'il existe un unique point H
sur (AB) tel que tout point K de (AB) vérifie NH≤NK. Calculer NH.

### Point de logique

La question 7 demande de « démontrer qu'il existe un unique X qui vérifie »
une certaine condition. C'est un genre de question qui revient souvent, donc
je vais en dire quelques mots.

D'abord, on se rappelle qu'il s'agit de deux questions séparées :
« démontrer qu'il existe un X qui » puis « démontrer que c'est le seul ».

Pour la deuxième partie, « démontrer que c'est le seul », on va commencer la
démonstration par « prenons-en un autre », et on va essaye de démontrer soit
« en fait c'est le même » soit « il ne vérifie pas la condition ».

Pour la première partie, il y a deux manières de s'y prendre: la manière
**constructive** et la manière **non constructive**.

La manière **constructive** consiste à trouver le X cherché. En gros, on
résout l'équation, et le fait d'avoir trouvé une solution est la preuve
qu'il y en a bien une. (Parfois, le raisonnement donne l'unicité par la même
occasion.)

Une variante de la manière constructive, c'est de *deviner* la solution, et
de prouver ensuite que c'en est bien une.

La manière **non constructive** est différente, quand on a fini on ne sait
toujours pas quel est le X cherché, on sait juste qu'il existe.

**Question pour vérifier que vous avez suivi jusqu'ici :** il y a un seul
gros exemple de méthode non constructive dans le cours, de quoi s'agit-il ?

Dans le cas de la question 7, la méthode qui marche est la méthode
constructive en parachutant la solution. Donc votre rédaction doit
ressembler à ceci :

Soit H le point *indiquez ici quel point vous pensez être le bon*.

Alors pour tout K sur (AB) *démontrez que NH≤NK*.

Donc H est bien le point cherché ; montrons qu'il est unique.

Soit H' sur (AB) différent de H *démontrez que H' ne vérifie pas la
condition*.

**Vérification de logique :** écrivez-moi la négation de la condition qui
définit H.
